import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActualizarAsignaturaComponent } from './actualizar-asignatura/actualizar-asignatura.component';
import { AsignaturaDetallesComponent } from './asignatura-detalles/asignatura-detalles.component';
import { ListaAsignaturasComponent } from './lista-asignaturas/lista-asignaturas.component';
import { RegistrarAsignaturaComponent } from './registrar-asignatura/registrar-asignatura.component';

const routes: Routes = [
  {path : 'asignaturas', component:ListaAsignaturasComponent},
  {path : '', redirectTo: 'asignaturas', pathMatch: 'full'},
  {path : 'registrar-asignatura', component : RegistrarAsignaturaComponent},
  {path : 'actualizar-asignatura/:id', component : ActualizarAsignaturaComponent},
  {path : 'asignatura-detalles', component : AsignaturaDetallesComponent}  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
