import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Asignatura } from '../asignatura';
import { AsignaturaService } from '../asignatura.service';

@Component({
  selector: 'app-registrar-asignatura',
  templateUrl: './registrar-asignatura.component.html',
  styleUrls: ['./registrar-asignatura.component.css']
})
export class RegistrarAsignaturaComponent implements OnInit {

  asignatura : Asignatura = new Asignatura();

  constructor(private asignaturaServicio: AsignaturaService, private router: Router) { }

  ngOnInit(): void {
    
  }
  
  guardarAsignatura(){
    this.asignaturaServicio.registrarAsignatura(this.asignatura).subscribe(dato =>{
      console.log(dato);
      this.irALaListaDeAsignatura();
    },error => console.log(error));
  }

  irALaListaDeAsignatura(){
    this.router.navigate(['/asignaturas'])
  }

  onSubmit(){
    //this.guardarAsignatura();
    console.log(this.asignatura)
  }
}
