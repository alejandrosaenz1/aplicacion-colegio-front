import { Component, OnInit } from '@angular/core';
import { Asignatura } from '../asignatura';
import { AsignaturaService } from '../asignatura.service';

@Component({
  selector: 'app-lista-asignaturas',
  templateUrl: './lista-asignaturas.component.html',
  styleUrls: ['./lista-asignaturas.component.css']
})
export class ListaAsignaturasComponent implements OnInit {

  asignaturas:Asignatura[];
  router: any;

  constructor(private asignaturaServicio: AsignaturaService) { }

  ngOnInit(): void {
    this.obtenerAsignaturas();
  }

  actualizarAsignatura(id:number){
    this.router.navigate(['actualizar-asignatura', id]);
  }

  eliminarAsignatura(id: number){
    this.asignaturaServicio.eliminarEmpleado(id).subscribe(dato =>{
      console.log(dato);
      this.obtenerAsignaturas;
    })
  }

  private obtenerAsignaturas(){
    this.asignaturaServicio.obtenerListaDeAsignaturas().subscribe(dato => {
      this.asignaturas = dato;
    })
  }

  verDetallesDeLaAsignatura(id:number){
    this.router.navigate(['asignatura-detalle', id])
  }
}
