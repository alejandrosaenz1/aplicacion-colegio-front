import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Asignatura } from '../asignatura';
import { AsignaturaService } from '../asignatura.service';

@Component({
  selector: 'app-actualizar-asignatura',
  templateUrl: './actualizar-asignatura.component.html',
  styleUrls: ['./actualizar-asignatura.component.css']
})
export class ActualizarAsignaturaComponent implements OnInit {
  id: number;
  asignatura:Asignatura = new Asignatura();
  constructor(private asignaturaService:AsignaturaService, private router:Router, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.asignaturaService.obtenerAsignaturaPorId(this.id).subscribe(dato =>{      
      this.asignatura = dato;      
    },error => console.log(error));
  }

  irAlaListaDeAsignatura(){
    this.router.navigate(['/asignaturas']);
    swal('Empleado actualizado',`La asignatura ${this.asignatura.nombre} ha sido actualizado con exito`,`success`);
  }

  onSubmit(){
    this.asignaturaService.actualizarAsignatura(this.id, this.asignatura).subscribe(dato => {
      this.irAlaListaDeAsignatura();
    },error => console.log(error));
  }

}


function swal(arg0: string, arg1: string, arg2: string) {
  throw new Error('Function not implemented.');
}

