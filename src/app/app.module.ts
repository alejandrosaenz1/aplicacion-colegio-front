import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListaAsignaturasComponent } from './lista-asignaturas/lista-asignaturas.component';
import { HttpClientModule} from '@angular/common/http';
import { RegistrarAsignaturaComponent } from './registrar-asignatura/registrar-asignatura.component';
import { FormsModule } from '@angular/forms';
import { ActualizarAsignaturaComponent } from './actualizar-asignatura/actualizar-asignatura.component';
import { AsignaturaDetallesComponent } from './asignatura-detalles/asignatura-detalles.component';

@NgModule({
  declarations: [
    AppComponent,
    ListaAsignaturasComponent,
    RegistrarAsignaturaComponent,
    ActualizarAsignaturaComponent,
    AsignaturaDetallesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
