import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Asignatura } from './asignatura';

@Injectable({
  providedIn: 'root'
})
export class AsignaturaService {

  //Esta Url Obtiene el listado de todas las asignaturas del backend
  private baseUrl = "http://localhost:8090/asignatura/listar";
  private baseUrlCrear = "http://localhost:8090/asignatura";
  private baseUrlObtener = "http://localhost:8090/asignatura"
  private baseUrlactualizar = "http://localhost:8090/asignatura";
  private baseUrlEliminar = "http://localhost:8090/asignatura";

  constructor(private httpClient : HttpClient) { }

  //Este metodo nos sirve para obtener las asignaturas
  obtenerListaDeAsignaturas():Observable<Asignatura[]>{
    return this.httpClient.get<Asignatura[]>(`${this.baseUrl}`);
  }
  //este metodo nos sirve para registrar una asignatura
  registrarAsignatura(asignatura : Asignatura) : Observable<Object>{
    return this.httpClient.post(`${this.baseUrlCrear}`, asignatura)
  }

  //este metodo sirve para actualizar la asignatura
  actualizarAsignatura(id:number, asignatura:Asignatura) : Observable<Object>{
    return this.httpClient.put(`${this.baseUrlactualizar}/${id}`,asignatura)
  }

  //este metodo nos sirve para obtener o buscar una asignatura
  obtenerAsignaturaPorId(id:number) : Observable<Asignatura>{
    return this.httpClient.get<Asignatura>(`${this.baseUrlObtener}/${id}`)
  }

  eliminarEmpleado(id:number) : Observable<Object>{
    return this.httpClient.delete(`${this.baseUrlEliminar}/${id}`);
  }
}
